import express from 'express';
import cors from 'cors';

const app = express();
app.use(cors());
app.get('/', (req, res) => {
  res.json({
    hello: 'JS World',
  });
});

app.get('/task2B', (req, res) => {
    //const sum = (+req.query.a || 0) + (+req.query.b || 0);
    const re = new RegExp('[0-9_\/\\~|\.,@#$^&\*\?\(\)]');
    const fio = req.query.fullname.split(' ');

    let resultString = '';
    let StringsInvalid = false;

    console.log(fio);
    for (var i = 0; i < fio.length; i++) {
        if (fio[i] == '') {fio.splice(i,1); i--;}
    }

    console.log(fio);

    for (var i = 0; i < fio.length; i++)
    {
        StringsInvalid = re.test(fio[i]);
        if (StringsInvalid) break;
    };


    if (!StringsInvalid) {
        switch(fio.length) {
            case 1:
                if (fio != '') {
                    resultString = fio[0].substr(0,1).toUpperCase() + fio[0].substr(1).toLowerCase();
                }
                else {
                    resultString = "Invalid fullname";
                }
                break;
            case 2:
                resultString = fio[1].substr(0,1).toUpperCase() +
                    fio[1].substr(1).toLowerCase() + ' ' + fio[0].substr(0,1).toUpperCase() + '.';
                break;
            case 3:
                resultString = fio[2].substr(0,1).toUpperCase() +
                    fio[2].substr(1).toLowerCase() + ' ' + fio[0].substr(0,1).toUpperCase() +
                    '. ' + fio[1].substr(0,1).toUpperCase() + '.';
                break;
            default :
                resultString = "Invalid fullname";
        }
    }
    else {
        resultString = "Invalid fullname";
    }
    return res.send(resultString);
});

app.listen(3000, () => {
  console.log('Your app listening on port 3000!');
});
